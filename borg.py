#!/usr/bin/python3
import json
import os
import sys
import subprocess
import multiprocessing
from datetime import datetime

import jinja2
import ldap

path = os.path.dirname(os.path.abspath(__file__))

class Repo:
    def __init__(self, borg_key, processes, count, users):
        self.borg_key = borg_key
        self.processes = processes
        self.count = count
        self.users = set(users)
        self.load()
        self._db = set(self.db)

    def config(self, user):
        config_path = os.path.join(path, 'generated', f'config.{ user }.yaml')
        with open(os.path.join(path,'templates','config.borg.yaml.j2')) as file:
            template = jinja2.Template(file.read())
            with open(config_path, 'w') as file:
                file.write(template.render(username=user, borg_key=self.borg_key))

    def _create(self, user):
        self.config(user)
        r = subprocess.run(['borgmatic', '-c',
            os.path.join(path, 'generated/', f'config.{ user }.yaml'),
            '--syslog-verbosity', '0', 'init', '-e', 'repokey'],
            capture_output=True)
        return r

    def create(self):
        with multiprocessing.Pool(self.processes) as pool:
            results = pool.map(self._create, self.new_users)
        for u,r in zip(self.new_users,results):
            if r.returncode == 0:
                self.db[u] = 0
            else:
                print(r.stderr.decode('utf-8'), file=sys.stderr)

    def _backup(self, user):
        self.config(user)
        r = subprocess.run(['borgmatic', '-c',
            os.path.join(path, 'generated/', f'config.{ user }.yaml'),
            '--syslog-verbosity', '0'])

    def backup(self):
        with multiprocessing.Pool(self.processes) as pool:
            results = pool.map(self._backup, self.pending_users)
        for u in self.pending_users:
            self.db[u] = datetime.now().timestamp()

    def purge(self):
        for user in self.old_users:
            del self.db[user]
            try:
                os.remove(os.path.join(path, 'generated/', f'config.{ user }.yaml'))
            except FileNotFoundError:
                pass

    @property
    def new_users(self):
        return self.users - self._db

    @property
    def old_users(self):
        return self._db - self.users

    @property
    def pending_users(self):
        return list(sorted(self.db.keys(), key=lambda u: self.db[u]))[:self.count]

    def load(self):
        try:
            with open(os.path.join(path,"db.json"), 'r') as db_file:
                self.db = json.load(db_file)
        except (IOError, ValueError) as e:
            self.db = {}

    def save(self):
        with open(os.path.join(path,"db.json"), 'w') as db_file:
            json.dump(self.db, db_file, indent=4)
        os.chmod(os.path.join(path,"db.json"), 0o600)

    def run(self):
        self.create()
        self.purge()
        self.backup()
        self.save()


def ldap_init(config):
    base = ldap.initialize(config['ldap']['server'])
    base.simple_bind_s(config['ldap']['binddn'], config['ldap']['password'])
    return base


def get_users(config, base):
    users_query_id = base.search(config['ldap']['rootdn'], ldap.SCOPE_ONELEVEL, 'objectClass=posixAccount')
    users = base.result(users_query_id)[1]
    return [ user['uid'][0].decode('utf-8') for _, user in users ]


if __name__ == '__main__':
    with open(os.path.join(path,"borg.json"), 'r') as config_file:
        config = json.load(config_file)
    base = ldap_init(config)
    users = get_users(config, base)
    repo = Repo(config['borg_key'], config['processes'], config['count'], users)
    repo.run()
